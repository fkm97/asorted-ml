import numpy as np
import matplotlib.pyplot as plt

def RMSE(t_test, t_predict):
    return np.sqrt(   np.sum(np.square(t_predict - t_test))/t_test.shape[0]   )

class NearestNeighborRegressor:

    def __init__(self, n_neighbors=1, dist_measure="euclidean", dist_matrix=None):

        self.n_neighbors = n_neighbors
        self.dist_measure = dist_measure
        self.dist_matrix = dist_matrix

    def fit(self, X, t):
        """
        Fits the nearest neighbor regression model.

        Parameters
        ----------
        X : Array of shape [n_samples, n_features]
        t : Array of length n_samples
        """

        self.X_train = X
        self.t_train = t


    def predict(self, X):
        """
        Computes predictions for a new set of points.

        Parameters
        ----------
        X : Array of shape [n_samples, n_features]

        Returns
        -------
        predictions : Array of length n_samples
        """
        # Given a data point, finds the distance to all points in the training set
        def find_euclid_dists(point):
            euclid = lambda row: np.sqrt(np.sum(np.square(row - point)))
            return np.apply_along_axis(euclid, 1, self.X_train)

        # Given a data point, finds the distance to all points in the training set
        def find_matrix_dists(point):
            M = self.dist_matrix
            def dist_matrix(p):
                p = p.reshape((-1, 1))
                q = point.reshape((-1, 1))
                return ((p - q).T @ M @ (p - q))[0]
            return np.apply_along_axis(dist_matrix, 1, self.X_train)

        # Given a list of distance to neighbors in the training set that respects the training sets order,
        # finds the index to the k-nearest neighbors.
        def find_neighbors(dists):
            return np.argsort(dists)[0:self.n_neighbors]

        # Given index to neighbors in t_train, calculates average target value of neighbors
        def calc_avg(neighbors):
            return np.average([t_train[i] for i in neighbors])

        # Calculates all the distances
        dists_func = (find_euclid_dists if self.dist_measure == "euclidean" else find_matrix_dists)
        # An array with all the dists to X_train for all the data points in X
        distss = np.apply_along_axis(dists_func, 1, X)
        neighborss = np.apply_along_axis(find_neighbors, 1, distss)
        return np.apply_along_axis(calc_avg, 1, neighborss)

def pop_row(i, X, t):
    return X[i].reshape((1, -1)), t[i], np.delete(X, i, 0), np.delete(t, i, 0)

def LOOCV(X, t, model):
    N, P = X.shape
    loss = 0
    for n in range(N):
        Xn, tn, Xmn, tmn = pop_row(n, X, t)
        model.fit(Xmn, tmn)
        t_predict = model.predict(Xn)[0]
        loss += (tn - t_predict)**2
    return loss / N
