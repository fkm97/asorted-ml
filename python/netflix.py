import numpy as np

def netflix_gradient(M, A, B):
    # This function is compute the gradient of E with respect to A and B
    # Given: * the (6 x 10) numpy array M, whose 0 inputs you want to replace
    #          by predictions, and whose nonzero inputs you want to preserve
    #        * two variable numpy arrays A (of size 6 x 2) and B (of size 2 x 10).
    # Return: The gradients grad_A and grad_B.


    def a(k, m):
        retval = 0
        for j in range(B.shape[1]):
            if M[k][j] != 0:
                for i in range(B.shape[0]):
                    retval += A[k][i]*B[i][j]*B[m][j]
                retval -= M[k][j]*B[m][j]
        return retval * 2

    def b(m, l):
        retval = 0
        for i in range(A.shape[0]):
            if M[i][l] != 0:
                for j in range(A.shape[1]):
                    retval += A[i][j]*A[i][m]*B[j][l]
                retval -= M[i][l]*A[i][m]
        return retval * 2

    def A_row(k):
        row = []
        for m in range(A.shape[1]):
            row.append(a(k[0], m))
        return np.array(row)

    def B_row(m):
        row = []
        for l in range(B.shape[1]):
            row.append(b(m[0], l))
        return np.array(row)

    grad_A = np.array(range(6)).reshape((-1, 1))
    grad_A = np.apply_along_axis(A_row, 1, grad_A)

    grad_B = np.array(range(2)).reshape((-1, 1))
    grad_B = np.apply_along_axis(B_row, 1, grad_B)


    return grad_A, grad_B

def E(A, B, M): #Loss function
    I_func = np.vectorize(lambda i, j: 0 if M[int(i)][int(j)] == 0 else 1)
    I = np.fromfunction(I_func, M.shape)

    return np.linalg.norm(I * (M - (A @ B)))**2


def netflix(M):
    # A function for performing gradient descent over A and B to minimize E
    # Input: The matrix M to be approximated
    people,movies = M.shape

    # Initialize at random
    np.random.seed(1)
    A = np.random.randn(people,2)
    B = np.random.randn(2,movies)

    # Set learning rate
    learningrate = 0.00001

    # Setting parameters for convergence check
    num_iter  = 1
    converged = False
    max_iter  = 10000
    tolerance = 0.01

    prev_loss = E(A, B, M)
    grad_A, grad_B = netflix_gradient(M, A, B)

    while not converged:
        A = A - (grad_A * learningrate)
        B = B - (grad_B * learningrate)

        grad_A, grad_B = netflix_gradient(M, A, B)

        #Adaptive learning rate
        cur_loss = E(A, B, M)
        if cur_loss < prev_loss:
            learningrate *= 1.05
        else:
            learningrate *= 0.95
        prev_loss = cur_loss

        num_iter = num_iter + 1
        cur_grad_norm = np.sqrt(np.linalg.norm(grad_A)**2 + np.linalg.norm(grad_B)**2)

        if cur_grad_norm < tolerance:
            converged = True
            print('converged')
        elif num_iter > max_iter:
            converged = True
            print('reached maximum nr of iterations')

    return A, B
